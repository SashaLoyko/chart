//
//  ViewController.swift
//  Chart
//
//  Created by Admin on 07.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SnapKit
import Charts



class ViewController: UIViewController {
    
   
    let mainView = UIView()
    let graphView = UIChartView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.updateChartData()
    }
    
    func setupUI(){
        self.view.addSubview(mainView)
        self.mainView.addSubview(graphView)
        
        mainView.backgroundColor = UIColor.white
        mainView.snp.makeConstraints{make in
            make.edges.equalToSuperview()
        }
        
        graphView.snp.makeConstraints{make in
            make.left.right.equalToSuperview().inset(10)
            make.top.bottom.equalToSuperview().inset(150)
        }
       
    }

   func updateChartData() {
        graphView.setDataCount(Int(10), range: UInt32(50))
    }

}

